Rails.application.routes.draw do
  get 'retrospective/index'
  match "/retrospective", to: "retrospective#index", :via => "get"

  get 'wedding/index'
  match "/wedding", to: "wedding#index", :via => "get"

  get 'about_me/index'
  match "/about_me", to: "about_me#index", :via => "get"

  get 'projects/index'
  match "/projects", to: "projects#index", :via => "get"

  get 'resume/index'
  match "/resume", to: "resume#index", :via => "get"

  get 'home/index'
  match "/home", to: "home#index", :via => "get"

  root 'home#index'
end
