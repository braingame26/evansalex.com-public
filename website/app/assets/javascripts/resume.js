//= require jquery
//= require jquery_ujs

$(document).ready(function() {
	$("#nav-resume").addClass("nav-current-item");
	
	$('[data-remodal-id=modal]').remodal();
	$('[data-remodal-id=lsretail-cicd]').remodal();
}); 