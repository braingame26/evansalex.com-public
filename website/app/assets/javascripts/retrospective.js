var tiles = [];

$(document).ready(function() {
	$(".grid-stack").gridstack();
	tiles = $(".grid-stack-item");

	$('.grid-stack').on('dragstart', function(event, ui) {
		var grid = this;
		var element = event.target;
		for(var i = 0; i < tiles.length; i++)
		{
			if (tiles.get(i) != element) {
				var grid = $('.grid-stack').data('gridstack');
				grid.removeWidget(tiles.get(i), false);
			}
		}
	});

	$('.grid-stack').on('dragstop', function(event, ui) {
		var grid = this;
		var element = event.target;
	});
}); 