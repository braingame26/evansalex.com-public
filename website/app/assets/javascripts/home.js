//= require jquery
//= require jquery_ujs

$(document).ready(function() {
	$(".carousel").slick({ 
		dots: true,
		slidesToShow: 1,
  		slidesToScroll: 1,
  		autoplay: true,
  		autoplaySpeed: 5000,
  		infinite: true,
  		arrows: false
	});
	$("#nav-home").addClass("nav-current-item");
}); 