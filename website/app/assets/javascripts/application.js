//= require jquery
//= require jquery_ujs

$(document).ready(function() {
	$('.toggle-nav').click(function(e) {
		$(this).toggleClass('active');
		$('.menu ul').toggleClass('active');
		e.preventDefault();
	});
	
	$( window ).resize(function() {
		if($(window).height() >= $("html").height())
		{
			$("#footer").css("position", "fixed");
		}
		else
		{
			$("#footer").css("position", "relative");
		}
	}).resize();
}); 